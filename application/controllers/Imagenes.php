<?php
class Imagenes extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->helper('file');
    }
    public function insertarfoto() {
    	$directorio = $this->input->post('directorio');
        $config['upload_path'] = '/var/www/html/cdn/imagenes/'.$directorio;
        $config['allowed_types'] = 'jpeg|jpg|png';//'jpeg|jpg|png';
        $config['max_size'] = '20000';
        $config['max_width'] = '4096 ';
        $config['max_height'] = '4096 ';
        $this->load->library('upload',$config);
        if (!$this->upload->do_upload("imagen")) {
            echo json_encode(
            	array(
	            	"exito" => false,
	            	"errores" => $this->upload->display_errors()
	            	)
        		);
        }else{
            $file_info = $this->upload->data();
            $nombre = $file_info['file_name'];
            echo json_encode(array("exito" => true,"nombre" => $nombre));
        }
    }
}